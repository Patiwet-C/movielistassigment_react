import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { Provider as SearchContextProvider } from './src/contexts/SearchContext'
import { Provider as FavouriteContextProvider } from './src/contexts/FavouriteContext'
import SearchScreen from './src/screen/search/SearchScreen';
import MovieScreen from './src/screen/movie/MovieScreen';
import DetailScreen from './src/screen/detail/DetailScreen';
import FavouriteScreen from './src/screen/favourite/FavouriteScreen';

const Stack = createStackNavigator()

const App = () => {
  return (
    <SearchContextProvider>
      <FavouriteContextProvider>
        <NavigationContainer>
          <Stack.Navigator initialRouteName='SearchScreen'>
            <Stack.Screen name='SearchScreen' component={SearchScreen} />
            <Stack.Screen name='MovieScreen' component={MovieScreen} />
            <Stack.Screen name='DetailScreen' component={DetailScreen} />
            <Stack.Screen name='FavouriteScreen' component={FavouriteScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </FavouriteContextProvider>
    </SearchContextProvider>

  )
}

export default App
