import React, { useContext } from 'react'
import { Button } from 'react-native'

import { Context as FavouriteContext } from '../contexts/FavouriteContext'
import { useNavigation } from '@react-navigation/native'

const FavouriteButton = () => {
    const navigation = useNavigation()
    const { state } = useContext(FavouriteContext)

    var isFavourite = false

    const checkFavourite = () => {
        if (state.length === 0) {
            isFavourite = true
        } else {
            isFavourite = false
        }
    }

    checkFavourite()

    return <Button
        disabled={isFavourite ? true : false}
        title="Favourite"
        onPress={() => {
            navigation.navigate('FavouriteScreen')
        }} />

}

export default FavouriteButton