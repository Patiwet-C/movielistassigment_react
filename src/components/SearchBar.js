import React, { useState, useContext } from 'react'
import { View, Button } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'

import styles from '../screen/search/Styles'
import { Context as SearchContext } from '../contexts/SearchContext'

const SearchBar = ({ navigation }) => {
    const [content, setContent] = useState();
    const { state, addSearchHistory, removeSearchHistory } = useContext(SearchContext)
    const searchHistorySize = 5

    const addSearch = () => {
        const searchItem = state.find((item) => item === content)

        if (state.length < searchHistorySize) {
            if (searchItem === undefined && content !== '') {
                addSearchHistory(content)
            } else {
                removeSearchHistory(content)
                addSearchHistory(content)
            }
        } else {
            removeSearchHistory(state[searchHistorySize - 1])
            addSearchHistory(content)
        }
    }

    return (
        <View style={styles.serchBarView}>
            <View style={styles.row}>
                <View style={styles.inputTextView}>
                    <TextInput
                        style={styles.defaultText}
                        placeholder="Search"
                        onChangeText={text => setContent(text)}
                        value={content}
                    />
                </View>
                <View style={styles.buttonView}>
                    <Button
                        title="Search"
                        onPress={() => {
                            addSearch(),
                            navigation.navigate('MovieScreen', { data: content }),
                                setContent('')
                        }}
                    />
                </View>
            </View>
        </View>

    )
}

export default SearchBar