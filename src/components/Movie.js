import React from 'react'
import { View, Image, Text } from 'react-native'
import styles from '../screen/movie/Styles'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'

const Movie = ({ item, navigation }) => {
    return (
        <TouchableWithoutFeedback
            style={styles.flatListSaperator}
            onPress={() => navigation.navigate('DetailScreen', { data: item })}>
            <View style={[styles.marginBox, styles.row]}>
                <Image
                    style={styles.imageView}
                    source={{ uri: `https://image.tmdb.org/t/p/w92${item.poster_path}` }}
                />
                <View>
                    <Text style={styles.titleText}>{item.title}</Text>
                    <Text style={styles.dateText}>{item.release_date}</Text>
                    <Text style={styles.defaultText} numberOfLines={4}>{item.overview}</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>

    )
}

export default Movie