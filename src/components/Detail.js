import React, { useContext } from 'react'
import { View, Text, Image, Button } from 'react-native'

import styles from '../screen/detail/Styles'
import { Context as FavouriteContext } from '../contexts/FavouriteContext'

const Detail = ({ data }) => {
    const { state, addFavourite, removeFavourite } = useContext(FavouriteContext)
    var isFavourite = false

    const checkFavourite = () => {
        if (state !== []) {
            state.map((item) => {
                if ((item.id === data.id)) {
                    isFavourite = true
                }
                else {
                    isFavourite = false
                }
            })
        }
    }

    checkFavourite()
    return (
        <View style={styles.container}>
            <Image
                style={[styles.imageView, styles.marginBox]}
                source={{ uri: `https://image.tmdb.org/t/p/w92${data.poster_path}` }} />
            <View style={styles.marginBox}>
                <Text style={styles.titleText}>{data.title}</Text>
            </View>
            <View style={styles.marginBox}>
                <Text style={styles.averageText}>Average Vote: {data.vote_average}</Text>
            </View>
            <View style={styles.marginBox}>
                <Text style={styles.defaultText}>{`\t${data.overview}`}</Text>
            </View>
            <View style={isFavourite ? styles.buttonFavourite : styles.buttonUnFavourite}>
                <Button
                    color={isFavourite ? '#E1E8ED' : '#1DA1F2'}
                    title={isFavourite ? 'Unfavourite' : 'Favourite'}
                    onPress={() => { isFavourite ? removeFavourite(data.id) : addFavourite(data) }}
                />
            </View>
        </View>
    )
}

export default Detail