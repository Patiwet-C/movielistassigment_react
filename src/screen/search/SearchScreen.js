import React, { useContext } from 'react'
import { View, Text } from 'react-native'
import { FlatList, TouchableWithoutFeedback } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'

import { Context as SearchContext } from '../../contexts/SearchContext'
import FavouriteButton from '../../components/FavouriteButton'
import SearchBar from '../../components/SearchBar'
import styles from './Styles'


const SearchScreen = () => {
    const { state, addSearchHistory, removeSearchHistory } = useContext(SearchContext)

    const navigation = useNavigation()
    navigation.setOptions({
        title: '',
        headerRight: () => (
            <FavouriteButton />
        )
    })

    return (
        <View style={styles.container}>
            <SearchBar navigation={navigation} />
            <FlatList
                data={state}
                keyExtractor={item => item}
                renderItem={({ item }) => {
                    return (
                        <TouchableWithoutFeedback
                            onPress={() => {
                                removeSearchHistory(item)
                                addSearchHistory(item)
                                navigation.navigate('MovieScreen', { data: item })
                            }}>
                            <View style={styles.flatListView}>
                                <Text style={styles.defaultText}>{item}</Text>
                            </View>
                            <View style={styles.flatListSaperator} />
                        </TouchableWithoutFeedback>

                    )

                }}
            />
        </View>
    )
}

export default SearchScreen