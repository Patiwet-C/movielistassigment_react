import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    flatListView: {
        justifyContent: 'center',
        marginTop: 8,
        height: 30,
        margin: 8,
    },
    row: {
        flexDirection: 'row'
    },
    defaultText: {
        fontSize: 16,
        marginLeft: 8,
    },
    serchBarView: {
        padding: 6,
        backgroundColor: '#B3B6B7'
    },
    inputTextView: {
        justifyContent: 'center',
        marginTop: 1,
        borderWidth: 1,
        borderRadius: 8,
        height: 35,
        width: '80%',
        backgroundColor: '#F7F9F9',
        borderColor: 'gray',
    },
    buttonView: {
        width: '20%',
    },
    flatListSaperator: {
        alignSelf: 'center',
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        width: '98%'
    },
})

export default styles