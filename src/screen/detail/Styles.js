import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
    },
    imageView: {
        alignSelf: 'center',
        width: 200,
        height: 300,
    },
    marginBox: {
        marginBottom: 16,
    },
    buttonFavourite: {
        marginTop: 24,
        borderWidth: 1,
        borderRadius: 32,
        backgroundColor: '#1DA1F2',
        borderColor: '#1DA1F2',
    },
    buttonUnFavourite: {
        marginTop: 24,
        borderWidth: 1,
        borderRadius: 32,
        backgroundColor: '#E1E8ED',
        borderColor: '#E1E8ED',
    },
    defaultText: {
        fontSize: 14,
    },
    averageText: {
        fontSize: 16,
    },
    titleText: {
        fontSize: 18,
        fontWeight: 'bold',
    },
})

export default styles