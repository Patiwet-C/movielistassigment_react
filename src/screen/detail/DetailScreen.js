import React from 'react'
import { Button } from 'react-native'
import { useNavigation } from '@react-navigation/native'

import Detail from '../../components/Detail'

const DetailScreen = props => {
    const { data } = props.route.params
    
    const navigation = useNavigation()
    navigation.setOptions({
        title:'',
        headerRight: () => (
            <Button 
            title='Back to search'
            onPress={() => navigation.navigate('SearchScreen')}
            />
        )
    })

    return <Detail data={data} />
}

export default DetailScreen