import React, { useContext } from 'react'
import { useNavigation } from '@react-navigation/native'
import { Context as FavouriteContext } from '../../contexts/FavouriteContext'
import { FlatList } from 'react-native-gesture-handler'
import { View } from 'react-native'

import styles from './Styles'
import Movie from '../../components/Movie'

const FavouriteScreen = () => {
    const navigation = useNavigation()
    const { state } = useContext(FavouriteContext)

    navigation.setOptions({
        title: '',
    })
    return (
        <View style={styles.container}>
            <FlatList
                data={state}
                keyExtractor={item => item.id.toString()}
                renderItem={({ item }) => (
                    <Movie
                        item={item}
                        navigation={navigation}
                    />
                )}
            />
        </View>
    )
}

export default FavouriteScreen