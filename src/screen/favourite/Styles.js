import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 8,
    },
    row: {
        flexDirection: 'row',
    },
    imageView: {
        width: 75,
        height: 125,
        marginRight: 20,
        marginLeft: 20,
    },
    marginBox: {
        marginBottom: 16,
    },
    listView: {
        flex: 1,
        justifyContent: 'space-between'
    },
    defaultText: {
        fontSize: 14,
    },
    dateText: {
        fontSize: 14,
        color: 'gray'
    },
    titleText: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    flatListSaperator: {
        alignSelf: 'center',
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        width: '98%'
    },
})

export default styles