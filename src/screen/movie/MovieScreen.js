import React, { useEffect, useState } from 'react'
import { View, Alert } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'

import { api as MovieAPI } from '../../api/MovieAPI'
import Movie from '../../components/Movie'
import styles from './Styles'


const MovieScreen = props => {
    const { data } = props.route.params
    const [movies, setMovies] = useState([])
    const navigation = useNavigation()
    var alertText = ''
    navigation.setOptions({
        title: '',
    })

    const callAlert = () => {
        Alert.alert(
            'Alert!!!',
            alertText,
            [
                {
                    text: 'OK',
                    onPress: () => navigation.navigate('SearchScreen')
                }
            ],
            { cancelable: false }
        );
    }


    const fetchMobile = async () => {
        const response = await MovieAPI.get('/api/movies/search', {
            params: {
                query: data,
                page: 1
            }
        });
        
        if (response.data.results.length === 0) {
            alertText = 'Movie not found'
            callAlert()
        } else if (alertText) {

        } else {
            setMovies(response.data)
        }

    };

    useEffect(() => {
        fetchMobile()
    }, [])

    return (
        <View style={styles.container}>
            <FlatList
                data={movies.results}
                keyExtractor={item => item.id.toString()}
                renderItem={({ item }) => {
                    return (
                        <View>
                            <Movie
                                item={item}
                                navigation={navigation}
                            />
                        </View>
                    )
                }}
            />
        </View>
    )
}

export default MovieScreen