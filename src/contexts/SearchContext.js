import { SEARCH_HISTORY, REMOVE_HOSTORY } from './constant'
import createDataContext from './createDataContext'

const searchReducer = (state, { type, payload }) => {
    switch (type) {
        case SEARCH_HISTORY:
            return [...state, payload].reverse()
        case REMOVE_HOSTORY:
            return state.filter((item) => item != payload)
        default:
            return state
    }
}

// Action
const addSearchHistory = dispatch => {
    return (content) => {
        dispatch({ type: SEARCH_HISTORY, payload: content })
    }
}

const removeSearchHistory = dispatch => {
    return (content) => {
        dispatch({ type: REMOVE_HOSTORY, payload: content })
    }
}

export const { Context, Provider } = createDataContext(
    searchReducer,
    { addSearchHistory, removeSearchHistory },
    []
)