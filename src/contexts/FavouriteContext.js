import { ADD_FAVOURITE, REMOVE_FAVOURITE } from './constant'
import createDataContext from './createDataContext'

const favouriteReducer = (state, { type, payload }) => {
    switch (type) {
        case ADD_FAVOURITE:
            return [...state, payload ]
        case REMOVE_FAVOURITE:
            return state.filter(favourite => favourite.id != payload);
        default:
            return state
    }
}

// Action

const addFavourite = dispatch => {
    return item => {
        dispatch({ type: ADD_FAVOURITE, payload: item })
    }
}

const removeFavourite = dispatch => {
    return id => {
        dispatch({ type: REMOVE_FAVOURITE, payload: id })
    }
}

export const { Context, Provider } = createDataContext(
    favouriteReducer,
    { addFavourite, removeFavourite },
    []
)